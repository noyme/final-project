<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('events','EventsController')->middleware('auth');
Route::get('events/delete/{id}', 'EventsController@destroy')->name('events.delete');
Route::get('events/changestatus/{cid}/{sid}', 'EventsController@changeStatus')->name('events.changestatus')->middleware('auth');
Route::get('events.view/{id}', 'EventsController@details')->name('events.view');
Route::get('bid.create/{id}', 'EventsController@createbid')->name('bid.create');
Route::get('events.index/{id}', 'EventsController@createbid')->name('bid.create');
Route::get('myevents', 'EventsController@myEvents')->name('myevents');
Route::get('mybids', 'EventsController@myBids')->name('mybids');
Route::get('bidapprove/{id}', 'EventsController@bidapproved')->name('bidapprove');

Route::get('bid.details/{id}', 'EventsController@biddetails')->name('bids.details');

Route::get('users/delete/{id}', 'UsersController@destroy')->name('users.delete');
Route::get('users.index', 'UsersController@index')->name('users.index');
Route::get('users.edit/{id}', 'UsersController@edit')->name('users.edit');
Route::post('users.update/{id}', 'UsersController@update')->name('users.update');
Route::get('users.create', 'UsersController@create')->name('users.create');
Route::post('users.store', 'UsersController@store')->name('users.store');
Route::get('users.customers', 'UsersController@customers')->name('users.customers');
Route::get('users.photographer', 'UsersController@photographer')->name('users.photographer');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
