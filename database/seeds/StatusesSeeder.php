<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert(
            [
                'name' =>  'Request Submited',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
    
            DB::table('statuses')->insert(
            [
                'name' => 'Bid Submited',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
    
             DB::table('statuses')->insert(
            [
                'name' => 'Bid approved',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            
            DB::table('statuses')->insert(
            [
                'name' => 'Bid Rejected',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            DB::table('statuses')->insert(
                [
                    'name' => 'Payment completed',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
    
    
        }
    
    
}
