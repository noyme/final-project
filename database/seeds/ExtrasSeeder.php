<?php

use Illuminate\Database\Seeder;
use\carbon\carbon;

class ExtrasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('extras')->insert([
            [
                'name' => 'Magnets',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Stills Photographer',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], 
            [
                'name' => 'An album for parents',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                
            ]);
    }

}
