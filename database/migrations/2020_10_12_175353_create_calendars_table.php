<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->string('contact_name_1', 100);
            $table->string('contact_email', 100);
            $table->string('contact_phone_number_1', 20);
            $table->string('contact_name_2', 100);
            $table->string('contact_phone_number_2', 100);
            $table->string('type',100);
            $table->string('location', 100);
            $table->integer('number_of_guests');
            $table->longText('notes', 500);
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars');
    }
}
