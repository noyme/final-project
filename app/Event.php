<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['date', 'contact_name_1', 'contact_email', 'contact_phone_number_1', 'contact_name_2', 'contact_phone_number_2', 'type', 'location', 'number_of_guests', 'notes', 'bid', 'bid_summary', 'status_id'];

    public function extras(){
        return $this->belongsToMany('App\Extra','event_extras');        
    }  

    public function users(){
        return $this->belongsToMany('App\User','event_users');        
    }  

    public function status(){
        return $this->belongsTo('App\Status', 'status_id');        
    }  

}    