<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventExtra extends Model
{
    protected $fillable = ['event_id','extra_id'];

}
