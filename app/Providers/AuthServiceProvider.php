<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('photographer', function ($user) {
            return $user->isPhotographer();
        });
        Gate::define('admin', function ($user) {
            return $user->isAdmin();
        });
        Gate::define('customer', function ($user) {
                return $user->isCustomer();
            });
        Gate::define('employee', function ($user) {
            if($user->isAdmin()) return TRUE;
            if($user->isPhotographer()) return TRUE;
            return (FALSE);
        });
    }
}
