<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    public function role(){
        return $this->belongsTo('App\Role', 'role_id');        
    }    

    public function events(){
        return $this->belongsToMany('App\User','event_users');        
    }  
    
    public function isAdmin(){
        $roles = $this->roles;
        if(!isset($user->roles)) return false;
        if($role->name === 'admin') return true;
        return false; 
    }

    public function isPhotographer(){
        
        $roles = $this->roles;
        if(!isset($user->roles)) return false;
        if($role->name === 'photographer') return true;
        return false; 
    }

    public function isCustomer(){
        $roles = $this->roles;
        if(!isset($roles)) {return true;}
        return false; 
    }



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
