<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Extra;
use App\EventExtra;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eve = Event::all();
        $statuses = Status::all();
        $events = $eve->sortByDesc('created_at');
        return view ('events.index', compact('events','statuses'));
    }

    public function bidindex()
    {
        $user = Auth::id();
        $event = Event::findOrFail($id);
        return view ('bids.index', compact('event'));
    }

    public function biddetails($id)
    {
        $event = Event::findOrFail($id);
        return view ('bids.details', compact('event'));
    }

    public function bidapproved($id)
    {
        $event = Event::findOrFail($id);
        $event->status_id = 3;
        $event->save();
        return view ('bids.index', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $extras = Extra::all();

        return view ('events.create', compact('extras'));
    }

    public function myEvents(){
        $user_id = Auth::id();
        $eve = DB::table('events')->where('user_id',$user_id)->pluck('id');
        $events = Event::findOrFail($eve)->all();
        return view('events.index', compact('events'));
    }

    
    public function myBids(){
        $user_id = Auth::id();
        $eve = DB::table('events')->where('user_id',$user_id)->pluck('id');
        $events = Event::findOrFail($eve)->all();
        return view('bids.index', compact('events'));
    }
    public function changestatus($eid,$sid){
        $event = Event::findOrFail($eid);     
        $candidate->status_id = $sid;
        $candidate->save();

            Session::flash('success', 'The status was change successfully');
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function details($id){
        $event = Event::findOrFail($id);
        $eventextras = DB::table('event_extras')->where('event_id',$event->id)->pluck('extra_id');
        $extras = Extra::findOrFail($eventextras)->all();
        $statuses = Status::all();
        return view ('events.view', compact('event','extras','statuses'));
    }

    public function createbid($id){
        $event = Event::findOrFail($id);
        $statuses = Status::all();
        return view ('bids.create', compact('event', 'statuses'));
    }

    public function store(Request $request)
    {
        $event = new Event();
        $eve = $event->create($request->all());
        $eve->user_id = Auth::id();
        $eve->status_id = 1;
        $eve->save();
        $extras = Extra::all();
            if($request->checkbox1 == TRUE){
                EventExtra::create([
                    'event_id' => $eve->id,
                    'extra_id' => 1,
                    ]);
            }
            if($request->checkbox2 == TRUE){
                EventExtra::create([
                    'event_id' => $eve->id,
                    'extra_id' => 2,
                    ]);
            }
            if($request->checkbox3 == TRUE){
                EventExtra::create([
                    'event_id' => $eve->id,
                    'extra_id' => 3,
                    ]);
            }

        return redirect('events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        $statuses = Status::all();
        return view ('events.edit', compact('event','statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        $event->update($request->all());
        $event->bid = $request->bid;
        $event->bid_summary = $request->bid_summary;
        $event->status_id = $request->status_id;
        return redirect('events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $event->delete();
        return redirect('events');
    }
}
