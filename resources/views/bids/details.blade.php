@extends('layouts.app')

@section('content')
                   

                    <div class="card">
                        <div class="card-header"></div>
        
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="date" class="col-md-4 col-form-label text-md-right">{{ __('Bid Amount') }}</label>
    
                                <div class="col-md-6">
                                    {{$event->bid}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date" class="col-md-4 col-form-label text-md-right">{{ __('Bid Details') }}</label>
    
                                <div class="col-md-6">
                                    {{$event->bid_summary}}
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">
                                {{ __('I Am Interested In') }}
                            </button>
                            <button type="submit" class="btn btn-primary">
                                <a herf = "{{route('bidapprove',$event->id)}}"> {{ __('Not Interested') }}</a>
                            </button>


                            <div class="form-group row">
                                <label for="date" class="col-md-4 col-form-label text-md-right"></label>
    
                                <div class="col-md-6">
                                    <a href = "{{route('events.view',$event->id)}}">View Event Details</a>
                                </div>
                            </div>
                               
                        </div>
                    </div>
                </div>
                @endsection
