@extends('layouts.app')

@section('content')
<h1>List of Bids</h1>
<table class = "table">
    <tr>
        <th>Event id</th><th>Event Date</th><th>Event Type</th><th>Event Location</th><th>Status</th><th>Bid</th><th>Bid Details</th>
    </tr>
    <!-- the table data -->
    @foreach($events as $event)
        <tr>
            <td>{{$event->id}}</td>
            <td>{{$event->date}}</td>
            <td>{{$event->type}}</td>
            <td>{{$event->location}}</td>
            <td>
                {{$event->status->name}}
            </td>
            <td>
                {{$event->bid}}
            </td>
            <td>
                <a href = "{{route('bids.details',$event->id)}}">Details</a>
            </td>

            <td>
                <a href = "{{route('events.view',$event->id)}}">View</a>
            </td>
        </tr>
    @endforeach
</table>

@endsection
