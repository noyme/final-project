@extends('layouts.app')

@section('content')
                   
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Submit bid') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('events.update', $event->id) }}">
                        @method('PATCH')
                        @csrf

                        <div class="form-group row">
                            <label for="date" class="col-md-4 col-form-label text-md-right">{{ __('Event Date') }}</label>

                            <div class="col-md-6">
                                {{$event->date}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="contact_name_1" class="col-md-4 col-form-label text-md-right">{{ __('Primary Contact Name') }}</label>

                            <div class="col-md-6">
                                {{$event->contact_name_1}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="contact_email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                {{$event->contact_email}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="contact_phone_number_1" class="col-md-4 col-form-label text-md-right">{{ __('Primary Contact Phone') }}</label>

                            <div class="col-md-6">
                                {{$event->contact_phone_number_1}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="contact_name_2" class="col-md-4 col-form-label text-md-right">{{ __('Secondery Contact Name') }}</label>

                            <div class="col-md-6">
                                {{$event->contact_name_2}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="contact_phone_number_2" class="col-md-4 col-form-label text-md-right">{{ __('Secondery Contact Phone') }}</label>

                            <div class="col-md-6">
                                {{$event->contact_phone_number_2}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('Event Type') }}</label>

                            <div class="col-md-6">
                                {{$event->type}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="location" class="col-md-4 col-form-label text-md-right">{{ __('Event Location') }}</label>

                            <div class="col-md-6">
                                {{$event->location}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="number_of_guests" class="col-md-4 col-form-label text-md-right">{{ __('Number Of Guests') }}</label>

                            <div class="col-md-6">
                                {{$event->number_of_guests}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="status_id" class="col-md-4 col-form-label text-md-right">Status</label>
                            <div class="col-md-6">
                                <select class="form-control" name="status_id">                                                                         
                                   @foreach ($statuses as $status)
                                   @if($status->id == 2)
                                     <option value="{{ $status->id }}" selected="selected"> 
                                         {{ $status->name }} 
                                     </option>
                                   @else
                                   <option value="{{ $status->id }}"> 
                                        {{ $status->name }} 
                                    </option>
                                   @endif                                      
                                   @endforeach    
                                 </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="notes" class="col-md-4 col-form-label text-md-right">{{ __('Notes') }}</label>

                            <div class="col-md-6">
                                {{$event->notes}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="bid" class="col-md-4 col-form-label text-md-right">{{ __('Bid') }}</label>

                            <div class="col-md-6">
                                <input id="bid" type="integer" class="form-control @error('email') is-invalid @enderror" name="bid" value="{{ old('bid') }}" required autocomplete="bid">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="bid_summary" class="col-md-4 col-form-label text-md-right">{{ __('Details') }}</label>

                            <div class="col-md-6">
                                <input id="bid_summary" type="longtext" class="form-control @error('email') is-invalid @enderror" name="bid_summary" value="{{ old('bid_summary') }}" required autocomplete="bid_summary">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('OK') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
