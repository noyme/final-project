@extends('layouts.app')
@section('title','List of Users')
@section('content')

@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
    <h1>List of Users</h1>
    <div><a href =  "{{route('users.create')}}"> New User</a></div>
    <div><a href =  "{{route('users.customers')}}"> View Customer Only</a></div>
    <div><a href =  "{{route('users.photographer')}}"> View Employees Only</a></div>
    <table class="table table-hover">
        <tr>
            <th>ID</th><th>Name</th><th>Email</th><th>Role</th><th>Edit</th><th>Delete</th>
        </tr>
        <!-- the table data-->
        @foreach($users as $user) 
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td> 
                @if(isset($user->role_id))
                <td>{{$user->role->name}}</td>
                @else
                <td></td>
                @endif 
   
                <td>
                    <a href = "{{route('users.edit',$user->id)}}">Edit</a>
                </td>
                <td>
                    <a href = "{{route('users.delete',$user->id)}}">Delete</a>
                </td>


            </tr>
        @endforeach
    </table>
@endsection