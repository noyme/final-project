@extends('layouts.app')
@section('title','List of Users')
@section('content')

@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
    <h1>List of Customers</h1>
    <div><a href =  "{{route('users.create')}}"> New User</a></div>
    <div><a href =  "{{route('users.customers')}}"> View Customer Only</a></div>
    <div><a href =  "{{route('users.photographer')}}"> View Employees Only</a></div>
    <table class="table table-hover">
        <tr>
            <th>ID</th><th>Name</th><th>Email</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th><th>Details</th>
        </tr>
        <!-- the table data-->
        @foreach($customers as $customer) 
            <tr>
                <td>{{$customer->id}}</td>
                <td>{{$customer->name}}</td>
                <td>{{$customer->email}}</td>   
                <td>{{$customer->created_at}}</td>
                <td>{{$customer->updated_at}}</td>
                <td>
                    <a href = "{{route('users.edit',$customer->id)}}">Edit</a>
                </td>
                <td>
                    <a href = "{{route('users.delete',$customer->id)}}">Delete</a>
                </td>


            </tr>
        @endforeach
    </table>
@endsection