@extends('layouts.app')

@section('content')
                   
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('update request') }}</div>
        
                        <div class="card-body">
                            <form method="POST" action="{{ route('events.update', $event->id) }}">
                                @method('PATCH')
                                @csrf
        
                                <div class="form-group row">
                                    <label for="date" class="col-md-4 col-form-label text-md-right">{{ __('Event Date') }}</label>
        
                                    <div class="col-md-6">
                                    <input id="date" type="date" class="form-control @error('date') is-invalid @enderror" name="date" value= "{{$event->date}}" required autocomplete="date" autofocus>
        
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="contact_name_1" class="col-md-4 col-form-label text-md-right">{{ __('Primary Contact Name') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="contact_name_1" type="text" class="form-control @error('email') is-invalid @enderror" name="contact_name_1" value="{{$event->contact_name_1}}" required autocomplete="contact_name_1">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="contact_email" type="email" class="form-control @error('email') is-invalid @enderror" name="contact_email" value="{{ $event->contact_email}}" required autocomplete="contact_email">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_phone_number_1" class="col-md-4 col-form-label text-md-right">{{ __('Primary Contact Phone') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="contact_phone_number_1" type="text" class="form-control @error('email') is-invalid @enderror" name="contact_phone_number_1" value="{{$event->contact_phone_number_1}}" required autocomplete="contact_phone_number_1">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_name_2" class="col-md-4 col-form-label text-md-right">{{ __('Secondery Contact Name') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="contact_name_2" type="text" class="form-control @error('email') is-invalid @enderror" name="contact_name_2" value="{{ $event->contact_name_2}}" required autocomplete="contact_name_2">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_phone_number_2" class="col-md-4 col-form-label text-md-right">{{ __('Secondery Contact Phone') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="contact_phone_number_2" type="text" class="form-control @error('email') is-invalid @enderror" name="contact_phone_number_2" value="{{ $event->contact_phone_number_2}}" required autocomplete="contact_phone_number_2">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('Event Type') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="type" type="text" class="form-control @error('email') is-invalid @enderror" name="type" value="{{$event->type}}" required autocomplete="type">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="location" class="col-md-4 col-form-label text-md-right">{{ __('Event Location') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="location" type="text" class="form-control @error('email') is-invalid @enderror" name="location" value="{{$event->location}}" required autocomplete="location">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="number_of_guests" class="col-md-4 col-form-label text-md-right">{{ __('Number Of Guests') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="number_of_guests" type="text" class="form-control @error('email') is-invalid @enderror" name="number_of_guests" value="{{$event->number_of_guests}}" required autocomplete="number_of_guests">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="status_id" class="col-md-4 col-form-label text-md-right">Status</label>
                                    <div class="col-md-6">
                                        <select class="form-control" name="status_id">                                                                         
                                           @foreach ($statuses as $status)
                                           @if($status->id == $event->status_id)
                                             <option value="{{ $status->id }}" selected="selected"> 
                                                 {{ $status->name }} 
                                             </option>
                                           @else
                                           <option value="{{ $status->id }}"> 
                                                {{ $status->name }} 
                                            </option>
                                           @endif                                      
                                           @endforeach    
                                         </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="notes" class="col-md-4 col-form-label text-md-right">{{ __('Notes') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="notes" type="text" class="form-control @error('email') is-invalid @enderror" name="notes" value="{{$event->notes}}" required autocomplete="notes">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                @if(isset($event->bid))
                                <div class="form-group row">
                                    <label for="bid" class="col-md-4 col-form-label text-md-right">{{ __('Bid') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="bid" type="text" class="form-control @error('email') is-invalid @enderror" name="bid" value="{{$event->bid}}" required autocomplete="bid">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="bid_summary" class="col-md-4 col-form-label text-md-right">{{ __('Bid Detailes') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="bid_summary" type="text" class="form-control @error('email') is-invalid @enderror" name="bid_summary" value="{{$event->bid_summary}}" required autocomplete="bid_summary">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                    
                                @endif
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Update') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
