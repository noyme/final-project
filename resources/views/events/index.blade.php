@extends('layouts.app')

@section('content')
                   
                    <div><a href =  "{{route('events.create')}}"> Submit Request</a></div>
                    <h1>List of Events</h1>
                    <table class = "table">
                        <tr>
                            <th>id</th><th>Date</th><th>Type</th><th>Location</th><th>Name</th><th>Phone</th><th>Status</th><th>Updated</th><th>Edit</th><th>Delete</th><th>View</th>
                        </tr>
                        <!-- the table data -->
                        @foreach($events as $event)
                            <tr>
                                <td>{{$event->id}}</td>
                                <td>{{$event->date}}</td>
                                <td>{{$event->type}}</td>
                                <td>{{$event->location}}</td>
                                <td>{{$event->contact_name_1}}</td>
                                <td>{{$event->contact_phone_number_1}}</td>
                                <td>{{$event->status->name}}</td>
                                <td>{{$event->created_at}}</td>

                                <td>
                                    <a href = "{{route('events.edit',$event->id)}}">Edit</a>
                                </td>
                                <td>
                                    <a href = "{{route('events.delete',$event->id)}}">Delete</a>
                                </td>
                                <td>
                                    <a href = "{{route('events.view',$event->id)}}">View</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>

@endsection
