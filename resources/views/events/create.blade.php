@extends('layouts.app')

@section('content')
                   
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Submit request') }}</div>
        
                        <div class="card-body">
                            <form method="POST" action="{{ route('events.store') }}">
                                @csrf
        
                                <div class="form-group row">
                                    <label for="date" class="col-md-4 col-form-label text-md-right">{{ __('Event Date') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="date" type="date" class="form-control @error('date') is-invalid @enderror" name="date" value="{{ old('date') }}" required autocomplete="date" autofocus>
        
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="contact_name_1" class="col-md-4 col-form-label text-md-right">{{ __('Primary Contact Name') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="contact_name_1" type="text" class="form-control @error('email') is-invalid @enderror" name="contact_name_1" value="{{ old('contact_name_1') }}" required autocomplete="contact_name_1">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="contact_email" type="email" class="form-control @error('email') is-invalid @enderror" name="contact_email" value="{{ old('contact_email') }}" required autocomplete="contact_email">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_phone_number_1" class="col-md-4 col-form-label text-md-right">{{ __('Primary Contact Phone') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="contact_phone_number_1" type="text" class="form-control @error('email') is-invalid @enderror" name="contact_phone_number_1" value="{{ old('contact_phone_number_1') }}" required autocomplete="contact_phone_number_1">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_name_2" class="col-md-4 col-form-label text-md-right">{{ __('Secondery Contact Name') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="contact_name_2" type="text" class="form-control @error('email') is-invalid @enderror" name="contact_name_2" value="{{ old('contact_name_2') }}" required autocomplete="contact_name_2">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_phone_number_2" class="col-md-4 col-form-label text-md-right">{{ __('Secondery Contact Phone') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="contact_phone_number_2" type="text" class="form-control @error('email') is-invalid @enderror" name="contact_phone_number_2" value="{{ old('contact_phone_number_2') }}" required autocomplete="contact_phone_number_2">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('Event Type') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="type" type="text" class="form-control @error('email') is-invalid @enderror" name="type" value="{{ old('type') }}" required autocomplete="type">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="location" class="col-md-4 col-form-label text-md-right">{{ __('Event Location') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="location" type="text" class="form-control @error('email') is-invalid @enderror" name="location" value="{{ old('location') }}" required autocomplete="location">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">

                                    @foreach ($extras as $extra)
                                        
                                    <label for="extra" class="col-md-4 col-form-label text-md-right"></label>
        
                                    <div class="col-md-6">
                                        <input type="checkbox" id="checkbox{{$extra->id}}" name="checkbox{{$extra->id}}" >
                                        <label for="checkbox{{$extra->id}}">{{$extra->name}}</label><br>
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    @endforeach
                                </div>

                                <div class="form-group row">
                                    <label for="number_of_guests" class="col-md-4 col-form-label text-md-right">{{ __('Number Of Guests') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="number_of_guests" type="text" class="form-control @error('email') is-invalid @enderror" name="number_of_guests" value="{{ old('number_of_guests') }}" required autocomplete="number_of_guests">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="notes" class="col-md-4 col-form-label text-md-right">{{ __('Notes') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="notes" type="text" class="form-control @error('email') is-invalid @enderror" name="notes" value="{{ old('notes') }}" required autocomplete="notes">
        
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Next') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
