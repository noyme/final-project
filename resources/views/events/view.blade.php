@extends('layouts.app')

@section('content')
                   
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Submit request') }}</div>
        
                        <div class="card-body">
                            <form method="post" action="{{ route('events.view', $event->id) }}">
                                @csrf
        
                                <div class="form-group row">
                                    <label for="date" class="col-md-4 col-form-label text-md-right">{{ __('Event Date') }}</label>
        
                                    <div class="col-md-6">
                                        {{$event->date}}
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="contact_name_1" class="col-md-4 col-form-label text-md-right">{{ __('Primary Contact Name') }}</label>
        
                                    <div class="col-md-6">
                                        {{$event->contact_name_1}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
        
                                    <div class="col-md-6">
                                        {{$event->contact_email}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_phone_number_1" class="col-md-4 col-form-label text-md-right">{{ __('Primary Contact Phone') }}</label>
        
                                    <div class="col-md-6">
                                        {{$event->contact_phone_number_1}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_name_2" class="col-md-4 col-form-label text-md-right">{{ __('Secondery Contact Name') }}</label>
        
                                    <div class="col-md-6">
                                        {{$event->contact_name_2}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="contact_phone_number_2" class="col-md-4 col-form-label text-md-right">{{ __('Secondery Contact Phone') }}</label>
        
                                    <div class="col-md-6">
                                        {{$event->contact_phone_number_2}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="type" class="col-md-4 col-form-label text-md-right">{{ __('Event Type') }}</label>
        
                                    <div class="col-md-6">
                                        {{$event->type}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="location" class="col-md-4 col-form-label text-md-right">{{ __('Event Location') }}</label>
        
                                    <div class="col-md-6">
                                        {{$event->location}}
                                    </div>
                                </div>
                                @foreach($extras as $extra)
                                <div class="form-group row">
                                    <label for="checkbox{{$extra->id}}" class="col-md-4 col-form-label text-md-right"></label><br>
                                    <div class="col-md-6">
                                        {{$extra->name}}
                                    </div>

    
                                </div>
                                @endforeach
                                <div class="form-group row">
                                    <label for="number_of_guests" class="col-md-4 col-form-label text-md-right">{{ __('Number Of Guests') }}</label>
        
                                    <div class="col-md-6">
                                        {{$event->number_of_guests}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="notes" class="col-md-4 col-form-label text-md-right">{{ __('Notes') }}</label>
        
                                    <div class="col-md-6">
                                        {{$event->notes}}
                                    </div>
                                </div>

                                @if(isset($event->bid))
                                <div class="form-group row">
                                    <label for="bid" class="col-md-4 col-form-label text-md-right">{{ __('Bid') }}</label>
        
                                    <div class="col-md-6">
                                        {{$event->bid}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="bid" class="col-md-4 col-form-label text-md-right">{{ __('Bid Details') }}</label>
        
                                    <div class="col-md-6">
                                        {{$event->bid_summary}}
                                    </div>
                                </div>
                                @else

                                <div>
                                    <a href = "{{route('bid.create',$event->id)}}">Submit bid</a>
                                </div>
                                @endif
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('OK') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
